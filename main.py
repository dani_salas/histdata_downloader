import requests
import re
from joblib import Parallel, delayed
import multiprocessing
import os
import sys, getopt
import datetime
import os
import zipfile

cpu_count = multiprocessing.cpu_count()
parallel = False
outputFolder = os.path.dirname(os.path.abspath(__file__)) + "/download"
createFolderBySymbol = False
months = range(1,13)
lastYear  =19
firstYear = 2
outputCsvFolder = outputFolder + "/csv"
deleteZip = False

def deleteTxtFiles(path):
    if os.path.exists(path):
        listOfFiles=os.listdir(path)
        for file in listOfFiles:
            if file.endswith(".txt"):
                os.remove(path+"/"+file)

def deleteZipFiles(path):
    if os.path.exists(path):
        listOfFiles=os.listdir(path)
        for file in listOfFiles:
            if file.endswith(".zip"):
                os.remove(path+"/"+file)                

def createFolder(folder):
    finalPath = outputFolder + folder
    if not os.path.exists(finalPath):
        os.makedirs(finalPath)

def unzip(pathfile,filename):
    print "Unziping... " + pathfile
    zip_ref = zipfile.ZipFile(pathfile, 'a')
    # createFolder(outputCsvFolder)
    zip_ref.extractall(outputCsvFolder+"/")
    zip_ref.close()

def getSymbols():
    symbols = requests.get("http://www.histdata.com/download-free-forex-data/?/ascii/tick-data-quotes")
    data = re.findall('<strong>(.{7})<\/strong>',symbols._content)
    dataFormated = []
    for d in data:
        dataFormated.append(d.replace('/',''))
    return dataFormated
def getTk(referer):

    tkRequest = requests.get(referer)
    data = re.findall('id="tk" value="(.+)"',tkRequest._content)

    if len(data)>0:
        return data[0]
    else:
        return 0

def donwloadData(year,datemonth,symbol):
    referer = 'http://www.histdata.com/download-free-forex-historical-data/?/ascii/tick-data-quotes/'+symbol.lower()+'/'+datemonth[0:4]+'/'+datemonth[4:]
    tk = getTk(referer)
    if tk != 0:
        headers = {
                    'Host': 'www.histdata.com',
                    'Connection': 'keep-alive',
                    'Content-Length': '104',
                    'Cache-Control': 'max-age=0',
                    'Origin': 'http://www.histdata.com',
                    'Upgrade-Insecure-Requests': '1',
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Referer': referer
                    }

        payload = {
            "date":"20"+format(year,"02"),
            "datemonth":datemonth,
            "platform":"ASCII",
            "timeframe":"T",
            "fxpair":symbol,
            "tk":tk
            }
        request = requests.post(url= "http://www.histdata.com/get.php", data=payload,headers = headers)
        if request.status_code == 200:
            filename = symbol.lower()+datemonth[0:4]+datemonth[4:]+".zip"
            createFile= outputFolder+"/"+filename
            with open(createFile,'wb') as fd:
                fd.write(request.content)
            print "Downloaded " + symbol.lower()+datemonth[0:4]+datemonth[4:]

            unzip(createFile,filename)
            if deleteZip:
                deleteZipFiles(outputFolder+"/"+filename)
    else:
        print "Not found " + symbol.lower()+datemonth[0:4]+datemonth[4:]
 
def parallelDownload(symbols):
    print "Parallel download"
    global lastYear 
    if firstYear == lastYear:
        lastYear = lastYear + 1
    for symbol in symbols:
        for year in range(firstYear,lastYear):
            results = Parallel(n_jobs=cpu_count)(delayed(donwloadData)(year,("20"+format(year,"02"))+format(month, "02"),str(symbol)) for month in months)
            deleteTxtFiles(outputCsvFolder)

def sequentialDownload(symbols):
    print "Sequential download"
    global lastYear 
    if firstYear == lastYear:
        lastYear = lastYear + 1
    for symbol in symbols:
        for year in range(firstYear,lastYear):
            for month in months:
                donwloadData(year,("20"+str(year))+format(month, "02"),str(symbol))
                deleteTxtFiles(outputCsvFolder)
            
if __name__ == "__main__":

    args = sys.argv[1:]
    for i in range(len(args)):
        if args[i] == "-p":
            parallel = args[i+1]
            print "Parallel value is: " + parallel
        elif args[i] == "-o":
            outputFolder = args[i+1]
            outputCsvFolder = outputFolder + "/csv"
            print "OutputFolder value is: " + outputFolder
        elif args[i]== "-c":
            createFolderBySymbol = args[i+1]
            print "Create Folder for each symbol is: " + createFolderBySymbol
        elif args[i]== "-y":
            firstYear = args[i+1]
            print "Download from year: " + firstYear            
        elif args[i]== "-d":
            deleteZip = args[i+1]
            print "Delete .zips"               
        else:
            "Bad parameter."

    symbols = getSymbols()
    createFolder("")
    if parallel:
        parallelDownload(symbols)
    else:
        sequentialDownload(symbols)

